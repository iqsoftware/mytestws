package example;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


/**
 * This is my simple SOAP based web service.
 * I had to add WebService to get it to work in process. !!!
 */

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface SquareRootServer
{
    @SuppressWarnings("unused")
    @WebMethod double getSquareRoot(double input);
    @SuppressWarnings("unused")
    @WebMethod String getTime();
}
