package example;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Created by Paul Bartlett on 18/05/2014. Updated today!
 * Hello!
 */

@WebService()
public class HelloWorld {
  @WebMethod
  public String sayHelloWorldFrom(String from) {
    String result = "Hello, world, from " + from;
    System.out.println(result);
    return result;
  }

    @WebMethod
    public String sayHelloWorld() {
        String result = "Hello, world";
        System.out.println(result);
        return result;
    }

    public static String sayHi()
  {
      return "Hi from the method";
  }

    /**
     * I have worked out that you just need to publish each webservice to a
     * different URL and then you have a locally hosted web server!
     * Not sure how it is hosted, but I guess Tomcat is running in-process.
     * @param argv
     */
  public static void main(String[] argv) {
    Object implementor = new HelloWorld ();
    String address = "http://localhost:9000/HelloWorld";
    Endpoint.publish(address, implementor);

    Object implementor2 = new SquareRootServerImpl();
    String address2 = "http://localhost:9000/SquareRoot";
    Endpoint.publish(address2, implementor2);


  }
}
