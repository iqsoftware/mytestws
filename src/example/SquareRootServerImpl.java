package example;

import javax.jws.WebService;
import java.util.Date;


/**
 * This is the implementation for the simple SOAP test web service
 */

@WebService(endpointInterface = "example.SquareRootServer")
public class SquareRootServerImpl implements SquareRootServer
{
    @SuppressWarnings("unused")
    public double getSquareRoot(double input)
    {
        return Math.sqrt(input);
    }

    @SuppressWarnings("unused")
    public String getTime()
    {
        return new Date().toString();
    }
}
